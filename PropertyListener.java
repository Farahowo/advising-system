package AdvisingSystem;

/**
 * Created by Farah on 7/16/2016.
 */
public interface PropertyListener {
    void onPropertyEvent(String name, int total);
}
