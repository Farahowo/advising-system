package AdvisingSystem;

import java.awt.*;

/**
 * Created by Farah on 7/16/2016.
 */
public class Beeper implements PropertyListener {

    Beeper(Registration reg) {
        reg.addPropertyListener(this);
    }
    @Override
    public void onPropertyEvent(String name, int total) {
        //Add BEEPER here
        if (name.equals("discount"))
            Toolkit.getDefaultToolkit().beep();
    }
}
