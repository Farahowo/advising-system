package AdvisingSystem;

import java.util.LinkedList;
//the registration database class
public class Registration {
    private LinkedList<Course> courseList;
    private int iterator;
    private IDiscountStrategy discountStrategy;
    private int[] selections;//
    private LinkedList<PropertyListener> listenerList = new LinkedList<>(); //PUB-SUB
    private boolean isFF = false; //is freedom fighter?

    Registration() {
        courseList = new LinkedList<>();
        iterator = 0;
    }

    public void addCourse(Course course) {
        courseList.add(course);
    }

    public int getTotal() {
        int total = 0;
        int size = courseList.size();

        for (int i=0;i<size;i++) {
            total += courseList.get(i).getSubTotal();
        }
        return total;
    }

    public int numOfCourse() {
        return courseList.size();
    }

    public Course getNextCourse() {
        return courseList.get(iterator++);
    }

    public Course getLastAdded() {
        return courseList.getLast();
    }

    public int getSerial() {
        return courseList.size();
    }

    public int getExtraFeeAmount() {
        IExtraFeeCalculator iefc = CourseFactory.getInstance().getExtraFeeCalculator();

        return iefc.getExtraAmount(this.getTotal());
    }

    public int getGrandTotal() {
        return this.getTotal() + this.getExtraFeeAmount();
    }

    //set the discount strategy selections
    public void setSelections(int[] selections) {
        this.selections = selections;
    }

    //PUB-SUB listener adder
    public void addPropertyListener(PropertyListener listener) {
        listenerList.add(listener);
    }
    //PUB-SUB publisher
    private void publishPropertyEvent(String name, int value) {
        for (int i=0;i<listenerList.size();i++) {
            listenerList.get(i).onPropertyEvent(name,value);
        }
    }

    //calculates discounted total according to combo box selection
    public void getTotalWithDiscount() {
        int total = 0;

        if (selections.length == 0)
            total = 0; //no discount

        boolean bestForNsu = false, bestForStudent = false;

        //composite selection decider
        for (int i=0;i<selections.length;i++) {
            if (selections[i] == 3)
                bestForNsu = true;
            else if (selections[i] == 4)
                bestForStudent = true;
        }

        if (bestForNsu) {
            CompositeDiscount cds = new BestForNSU();

            for (int i=0;i<selections.length-1;i++) {//add all the applicable strategies
                if (selections[i]==0)
                    discountStrategy = new AcademicExcellenceDiscount();
                else if (selections[i] == 1) {
                    isFF = true;
                    discountStrategy = new FreedomFighterDiscount();
                }
                else if (selections[i] == 2)
                    discountStrategy = new AboriginalGroupDiscount();
                cds.add(discountStrategy);
            }
            total = cds.getTotal(this);
        } else if (bestForStudent) {
            CompositeDiscount cds = new BestForStudent();

            for (int i=0;i<selections.length-1;i++) {//add all the applicable strategies
                if (selections[i]==0)
                    discountStrategy = new AcademicExcellenceDiscount();
                else if (selections[i] == 1) {
                    discountStrategy = new FreedomFighterDiscount();
                    isFF = true;
                }
                else if (selections[i] == 2)
                    discountStrategy = new AboriginalGroupDiscount();
                cds.add(discountStrategy);
            }
            total = cds.getTotal(this);
        } else {
            if (selections[0]==0)
                discountStrategy = new AcademicExcellenceDiscount();
            else if (selections[0] == 1) {
                discountStrategy = new FreedomFighterDiscount();
                isFF = true;
            }
            else if (selections[0] == 2)
                discountStrategy = new AboriginalGroupDiscount();
            total = discountStrategy.getTotal(this);
        }

        this.publishPropertyEvent("discount",total); //PUB-SUB
    }

    //check whether user has FF advantage and taken more than five courses
    public void checkFiveCourse() {
        if (isFF && numOfCourse() > 5) {
            publishPropertyEvent("FreedomFighter",0);
            isFF = false;
        }
    }
    //custom method//
    public void printAllRegistered() {
        Course temp;

        for (int i=0;i<courseList.size();i++) {
            temp = new Course();
            temp = courseList.get(i);
            System.out.println(temp.getId()+" "+temp.getTitle());
        }
    }
}
