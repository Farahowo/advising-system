package AdvisingSystem;

/**
 * Created by mansib on 7/2/16.
 */
public class FreedomFighterDiscount implements IDiscountStrategy{
    @Override
    public int getTotal(Registration registration) {
        int temp = registration.getGrandTotal();
        return temp - 20000;
    }
}
