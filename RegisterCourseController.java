package AdvisingSystem;

public class RegisterCourseController {
    private Registration reg;
    private CourseFactory cFac;

    RegisterCourseController() {
        reg = new Registration();
        cFac = new CourseFactory();
    }

    public void makeNewRegistration() {
        reg = new Registration();
    }

    public void addCourse(String id) {
        Course temp = this.getCourse(id);

        if (temp!=null)
            reg.addCourse(temp);
    }

    public Registration getRegistration() {
        return reg;
    }

    public Course getCourse(String id) {
        return cFac.getCourse(id);
    }

    public void newCourse(Course course) {
        //NEW DATA
        cFac.newCourse(course);
    }

    //PROXY
    public IProgram getProgram(String courseId) {
        return cFac.getProgram(courseId);
    }
}
