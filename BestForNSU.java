package AdvisingSystem;

/**
 * Created by mansib on 7/2/16.
 */
public class BestForNSU extends CompositeDiscount {
    @Override
    public int getTotal(Registration registration) {
        int max = -1, temp = 0;

        for (int i=0;i<super.slist.size();i++) {
            temp = super.slist.get(i).getTotal(registration);

            if(temp > max)
                max = temp;
        }
        return max;
    }
}
