package AdvisingSystem;

/**
 * Created by mansib on 8/2/16.
 */
public interface IProgram {
    public String getName();
    public String getDepartment();
}
