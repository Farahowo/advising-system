package AdvisingSystem;

/**
 * Created by mansib on 8/2/16.
 */
public class ProgramProxy implements IProgram {
    private IProgram realSubject;
    private PersistenceFacade pFacade; //
    private int oid; //

    ProgramProxy() {
        realSubject = null;
        pFacade = new PersistenceFacade(); //
    }

    public void searchOid(int oid) {
        this.oid = oid;
    }

    private IProgram getRealSubject() {
        if (realSubject == null) {
            realSubject = pFacade.get(oid);
        }

        return realSubject;
    }

    @Override
    public String getName() {
        return getRealSubject().getName();
    }

    @Override
    public String getDepartment() {
        return getRealSubject().getDepartment();
    }
}
