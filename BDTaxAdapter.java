package AdvisingSystem;

/**
 * Created by mansib on 6/21/16.
 */
public class BDTaxAdapter implements IExtraFeeCalculator {
    BDTaxCalculator bdtax = new BDTaxCalculator();

    @Override
    public int getExtraAmount(int courseTotal) {
        return (int)Math.ceil(bdtax.calculateVATAmount(courseTotal));
    }
}
