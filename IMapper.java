package AdvisingSystem;

/**
 * Created by mansib on 7/27/16.
 */
public interface IMapper {
    Object get(int oid);
    void put(int oid, Object obj);
}
