package AdvisingSystem;

public interface IDiscountStrategy {
    public int getTotal (Registration registration);
}