package AdvisingSystem;

import java.util.LinkedList;

/**
 * Created by mansib on 7/2/16.
 */
public class CompositeDiscount implements IDiscountStrategy{
    public LinkedList<IDiscountStrategy> slist;

    CompositeDiscount() {
        slist = new LinkedList<IDiscountStrategy>();
    }

    public void add(IDiscountStrategy ids) {
        slist.add(ids);
    }

    public int getTotal(Registration registration) {
        return 0; //
    }
}
