package AdvisingSystem;

public class Course {
    private int oid; //object ID
    private String id;
    private String title;
    private int credit;
    private int tuitionPerCredit;
    private Program program; //For virtual proxy

    Course() {
        id = "No ID";
        title = "No name";
        credit = 0;
        tuitionPerCredit = 0;
    }

    public void setOid(int oid) {
        this.oid = oid;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setTuitionPerCredit(int tuitionPerCredit) {
        this.tuitionPerCredit = tuitionPerCredit;
    }

    public int getOid() {
        return oid;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getCredit() {
        return credit;
    }

    public int getTuitionPerCredit() {
        return tuitionPerCredit;
    }

    public int getSubTotal() {
        return credit * tuitionPerCredit;
    }
}
