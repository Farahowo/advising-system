package AdvisingSystem;

/**
 * Created by mansib on 7/2/16.
 */
public class BestForStudent extends CompositeDiscount {
    @Override
    public int getTotal(Registration registration) {
        int min = 100000, temp = 0;

        for (int i=0;i<super.slist.size();i++) {
            temp = super.slist.get(i).getTotal(registration);

            if(temp < min)
                min = temp;
        }
        return min;
    }
}
