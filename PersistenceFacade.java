package AdvisingSystem;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by mansib on 8/2/16.
 */
//This class is not fully implemented
public class PersistenceFacade {
    //private LinkedList<IProgram> pList;
    private HashMap<Integer,IProgram> pList;

    PersistenceFacade() {
        //hard coding program list for course from DB
        Program p = new Program();
        pList = new HashMap<>();

        p.setName("CSE");
        p.setDepartment("Department of ECE");
        pList.put(1996909600,p);

        p = new Program();
        p.setName("CSE");
        p.setDepartment("Department of ECE");
        pList.put(1996910592,p);

        p = new Program();
        p.setName("EEE");
        p.setDepartment("Department of ECE");
        pList.put(2041238604,p);

        p = new Program();
        p.setName("ECO");
        p.setDepartment("Department of Economics");
        pList.put(2039689441,p);

        p = new Program();
        p.setName("ENG");
        p.setDepartment("Department of English");
        pList.put(2049609846,p);
    }

    public IProgram get(int oid) {
        return pList.get(oid);
    }
}
