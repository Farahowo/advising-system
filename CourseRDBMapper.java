package AdvisingSystem;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by mansib on 7/27/16.
 */
public class CourseRDBMapper extends AbstractRDBMapper {
    //String tableName;

    CourseRDBMapper(String tableName) {
        super(tableName);
    }

    @Override
    protected Object GetObjectFromRecord(int oid, ResultSet DBRecord) {
        Course c = new Course();

        c.setOid(oid);

        try {
            c.setId(DBRecord.getString(2));
            c.setTitle(DBRecord.getString(3));
            c.setCredit(Integer.parseInt(DBRecord.getString(4)));
            c.setTuitionPerCredit(Integer.parseInt(DBRecord.getString(5)));

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }

    @Override
    protected void PutObjectInRecord(int oid, Object obj) {

    }
}
