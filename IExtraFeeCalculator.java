package AdvisingSystem;

public interface IExtraFeeCalculator {
    public int getExtraAmount(int courseTotal);
}