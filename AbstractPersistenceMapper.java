package AdvisingSystem;

/**
 * Created by mansib on 7/27/16.
 */
public abstract class AbstractPersistenceMapper implements IMapper {
    @Override
    public Object get(int oid) {
        return GetObjectFromStorage(oid);
    }

    @Override
    public void put(int oid, Object obj) {
        PutObjectInStorage(oid, obj); //NEW DATA
    }

    protected abstract Object GetObjectFromStorage(int oid);

    protected abstract void PutObjectInStorage(int oid, Object obj); //NEW DATA
}
