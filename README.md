# DISCLAIMER: Due to GitLab only allowing the repository to be viewed under the Maintainer's account and not the Developers' account this repository has been cloned and reuploaded to show up under all Developers' repository list.
# Advising-system Software
*CSE327 Project*
### An application which lets program coordinators add courses to the database and lets students navigate through all the available courses. A course is closed for registration once all the seats are booked by students. The application calculates the total tuition fee per semester and takes addtional factors like Freedom Fighter discount, taxes etc. into account.

**Developers' names-**
1. Md. Asimuzzaman Mansib (@AMansib)
2. Farah Hossain (@Farahowo)
