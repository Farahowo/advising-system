package AdvisingSystem;

import java.sql.*;

/**
 * Created by mansib on 7/27/16.
 */
public abstract class AbstractRDBMapper extends AbstractPersistenceMapper{
    private static String tableName;

    AbstractRDBMapper(String tableName) {
        this.tableName = tableName;
    }

    protected Object GetObjectFromStorage(int oid){
        ResultSet dbRec = GetDBRecord(oid);

        return GetObjectFromRecord(oid,dbRec);
    }

    //NEW DATA
    @Override
    protected void PutObjectInStorage(int oid, Object obj) {
        PutDBRecord(oid, (Course)obj); //CHECK

        PutObjectInRecord(oid,obj);
    }

    protected abstract Object GetObjectFromRecord(int oid, ResultSet DBRecord);

    protected abstract void PutObjectInRecord(int oid, Object obj); //NEW DATA

    private ResultSet GetDBRecord(int oid) { //STRING MAY CHANGE
        try {
            CreateTable();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String key = Integer.toString(oid);
        ResultSet DBRecord = Select(oid);

        return DBRecord;
    }

    private void PutDBRecord(int oid, Course course) {
        try {
            CreateTable();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            post(oid, course);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ResultSet Select(int oid) {
        try{
            Connection con = getConnection();
            String query = "SELECT * FROM `mydb`.`"+ tableName+ "` WHERE " + "OID = " + oid;
            PreparedStatement statement = con.prepareStatement(query); //example

            ResultSet result = statement.executeQuery();

            //ArrayList<String> arr = new ArrayList<String>();
            /*while (result.next()){
                System.out.println(result.getString("ID")); //Example
                System.out.println(" "); //same
                System.out.println(result.getString("TITLE")); //same

                arr.add(result.getString("ID"));

            }*/
            //System.out.println("YAY!");
            //con.close();//
            result.next(); //otherwise, error: java.sql.SQLException: Before start of result set
            return result;
        } catch (Exception e){
            System.out.println(e);

            return null;
        } finally {
            try {
                //getConnection().close();
            } catch (Exception e) {
                e.printStackTrace();
                //return null;
            }
        }
    }

    private static Connection getConnection() {
        try{
            String driver = "com.mysql.jdbc.Driver";
            //String url = "jdbc:mysql://127.0.0.1:3306/mydb?autoReconnect=true&useSSL=false"; //or localhost in lieu of ip address
            String url = "jdbc:mysql://localhost:3306/mydb?autoReconnect=true&useSSL=false";
            String username = "root";
            String password = "password";
            //String password = "";
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(url,username,password);
            //if(conn != null)System.out.println("Connected");
            return conn;
        } catch(Exception e){
            System.out.println(e);
        }
        return null;
    }

    private static void CreateTable() throws Exception{
        try{
            Connection con = getConnection();
            String query = "CREATE TABLE IF NOT EXISTS `mydb`.`"+ tableName+ "`(OID INT, ID varchar(10), TITLE varchar(30), CREDIT int, TUITION int, PRIMARY KEY(OID));";
            PreparedStatement create = con.prepareStatement(query); //write the query
            //ADD FOR COST!!!!
            create.executeUpdate();
            //con.close();//
        } catch (Exception e){
            System.out.println(e);
        }
        finally {
            //System.out.println("Function Complete?");
            //getConnection().close();
        }
    }

    public static void post(int oid, Course course) throws Exception {
        final String id = course.getId(); //isn't this suppose to be dynamic?
        final String title = course.getTitle();
        final int credit = course.getCredit();
        final int tuition = course.getTuitionPerCredit(); //CHECK TYPE!

        try {
            Connection con = getConnection();
            String query = "INSERT INTO `mydb`.`"+tableName+"` VALUES (\'" + oid + "\', \'" + id + "\', \'" + title + "\', \'" + credit + "\', \'" + tuition + "\')";
            PreparedStatement posted = con.prepareStatement(query);
            //table name is 'table'
            posted.executeUpdate();

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            System.out.println("Insert Complete.");

        }

    }

}
