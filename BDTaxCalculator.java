package AdvisingSystem;

/**
 * Created by mansib on 6/21/16.
 */
public class BDTaxCalculator {
    BDTaxCalculator() {
        //
    }

    public float calculateVATAmount(int total) {
        return (float)(total * 0.15);
    }
}
