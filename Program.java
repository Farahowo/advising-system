package AdvisingSystem;

/**
 * Created by mansib on 8/2/16.
 */
public class Program implements IProgram {
    private String name;
    private String department;

    Program() {
        name = null;
        department = null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDepartment(String department) {
        this.department = department;
    }


    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }
}
