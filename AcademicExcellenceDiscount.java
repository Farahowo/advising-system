package AdvisingSystem;

/**
 * Created by mansib on 7/2/16.
 */
public class AcademicExcellenceDiscount implements IDiscountStrategy {
    @Override
    public int getTotal(Registration registration) {
        int temp = registration.getGrandTotal();
        return (int)(temp*0.5);
    }
}
