package AdvisingSystem;

/**
 * Created by mansib on 6/21/16.
 */
public class DevelopmentFeeCalculator implements IExtraFeeCalculator {
    @Override
    public int getExtraAmount(int courseTotal) {
        return (int)Math.ceil(courseTotal*0.1);
    }
}
