package AdvisingSystem; /**
 * Created by Farah on 6/12/2016.
 */

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GUI extends JFrame implements PropertyListener {
    private static final int WIDTH = 800;
    private static final int HEIGHT = 550;
    private RegisterCourseController rcc;

    private int grandTotal = 0; //PUB-SUB variable

    private DefaultTableModel tableModel;
    private JTable table;
    private static final String CalString = "Calculate Discount";
    private JList list;
    private DefaultListModel listModel;


    // private final static boolean shouldFill = true;
    //private final static boolean shouldWeightX = true;
    //private final static boolean RIGHT_TO_LEFT = false;

    private JLabel CourseId, widthL, areaL, perimeterL;
    public JTextField InputcourseTF, widthTF, areaTF, perimeterTF, IdTF, TitleTF, CreditTF, TuitionperCreTF;
    private JLabel Discount, GrandTotal, Tax, Total, widthT, areaT, perimeterT, IdL, TitleL, CreditL, TuitionPerCreditL; //
    private JButton CalDiscountB, RegistrationB, AddcourseB, ReturnToRegB, NewCourseB, CourseDescriptionB;
    //private JPanel NewRegistrationP;

    private RegistrationButtonHandler rbHandler;
    private AddCourseButtonHandler acbHandler;
    private CalDiscountListener cdbListener;
    private NewCourseButtonHandler ncbHandler;
    private CourseDesButtonHandler cdesbHandler;
    //private ReturnToRegHandler rtrbHandler;


    public GUI() {
        rcc = new RegisterCourseController();
        rcc.getRegistration().addPropertyListener(this); //PUB-SUB add listener
        CourseId = new JLabel("Course ID");
        Total = new JLabel("Total = 0"); //
        Tax = new JLabel("Development Fee / BD Tax = 0");
        GrandTotal = new JLabel("Grand Total = 0");
        Discount = new JLabel("Discount = 0");

        InputcourseTF = new JTextField(10);

        // widthL = new JLabel("Enter the width: ", SwingConstants.RIGHT);
        //areaL = new JLabel("Area: ", SwingConstants.RIGHT);
        //perimeterL = new JLabel("Perimeter: ", SwingConstants.RIGHT);

        //widthTF = new JTextField(10);
        //areaTF = new JTextField(10);
        //perimeterTF = new JTextField(10);

        //NewRegistrationP = new JPanel();             //
        //NewRegistrationP.setLayout(new GridLayout());           //

        RegistrationB = new JButton("New Registration");
        rbHandler = new RegistrationButtonHandler();
        RegistrationB.addActionListener(rbHandler);

        AddcourseB = new JButton("Add Course");
        acbHandler = new AddCourseButtonHandler();
        AddcourseB.addActionListener(acbHandler);

        CalDiscountB = new JButton(CalString);
        CalDiscountB.setActionCommand(CalString);
        cdbListener = new CalDiscountListener();
        CalDiscountB.addActionListener(cdbListener);

        NewCourseB = new JButton("Add New Course");
        ncbHandler = new NewCourseButtonHandler();
        NewCourseB.addActionListener(ncbHandler);

        CourseDescriptionB = new JButton("Course Description");
        cdesbHandler = new CourseDesButtonHandler();
        CourseDescriptionB.addActionListener(cdesbHandler);

        setTitle("Advising Now Open");

        /*NewRegistrationP.add(RegistrationB);

       Container pane = getContentPane();
        pane.setLayout(new GridLayout(5, 2));

       NewRegistrationP.add(RegistrationB);

        pane.add(NewRegistrationP);
        pane.setLocation(350, 300);

        pane.add(lengthL);
        pane.add(lengthTF);
        pane.add(widthL);
        pane.add(widthTF);
        pane.add(areaL);
        pane.add(areaTF);
        pane.add(AddcourseB);*/

        GridBagLayoutDemo GBLD = new GridBagLayoutDemo();
        GBLD.addComponentsToPane(getContentPane());

        //tableModel = GBLD.getTableModel();

        setSize(WIDTH, HEIGHT);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public class GridBagLayoutDemo {
        final static boolean shouldFill = true;
        final static boolean shouldWeightX = true;
        final static boolean RIGHT_TO_LEFT = false;
        //private DefaultTableModel tableModel; //

        public void addComponentsToPane(Container pane) {
            if (RIGHT_TO_LEFT) {
                pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            }

            //JButton button;
            pane.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            if (shouldFill) {
                //natural height, maximum width
                c.fill = GridBagConstraints.HORIZONTAL;
            }

            //button = new JButton("Button 1");
            if (shouldWeightX) {
                c.weightx = 0.0;
            }
            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(0, 10, 10, 10);
            c.gridx = 0;
            c.gridy = 0;
            pane.add(RegistrationB, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.insets = new Insets(0, 300, 10, 10);
            c.gridx = 1;
            c.gridy = 0;
            pane.add(NewCourseB, c);

            //button = new JButton("Button 2");
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 10, 0);
            c.gridx = 0;
            c.gridy = 1;
            pane.add(CourseId, c);

            //button = new JButton("Button 3");
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.5;  // 0.0??
            c.gridwidth = 2;
            c.insets = new Insets(10, 0, 10, 10);
            c.gridx = 1;
            c.gridy = 1;
            pane.add(InputcourseTF, c);

            //button = new JButton("Long-Named Button 4");
            c.fill = GridBagConstraints.WEST;
            //c.ipady = 40;      //make this component tall
            c.weightx = 0.0;
            c.insets = new Insets(10, 0, 10, 0);
            c.gridx = 0;
            //c.gridwidth = 2;   //2.5 columns wide
            c.gridy = 2;
            pane.add(AddcourseB, c);

            //CourseDescriptionB
            c.fill = GridBagConstraints.EAST;
            //c.ipady = 40;      //make this component tall
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 10, 10);
            c.gridx = 1;
            //c.gridwidth = 2;   //2.5 columns wide
            c.gridy = 2;
            pane.add(CourseDescriptionB, c);


            //button = new JButton("5");
            //create table with data
            String[] columns = new String[]{
                    "SL#", "Course Title", "Credit", "Tuition/Credit", "Sub Total"
            };
            Registration registrationDb = rcc.getRegistration();
            int numOfRows = registrationDb.numOfCourse();

            Object[][] data = new Object[0][5];

            /*Course temp;
            int subtotal = 0;

            DefaultTableModel tableModel = new DefaultTableModel(data, columns);
            table = new JTable(tableModel);
            //add(new JScrollPane(table));
            //tableModel = (DefaultTableModel)table.getModel();
            //pack();
            //table.setEnabled(true);
            setVisible(true);*/

            tableModel = new DefaultTableModel(data, columns);
            JTable Table = new JTable(tableModel);
            Table.setPreferredScrollableViewportSize(new Dimension(350, 100));
            Table.setFillsViewportHeight(true); //makes the table background white?
            JScrollPane scrollTable = new JScrollPane(Table);
            pack();

            TableColumn column = null;
            for (int i = 0; i < 5; i++) {
                column = Table.getColumnModel().getColumn(i);
                if (i == 1) {
                    column.setPreferredWidth(175); //third column is bigger
                } else {
                    column.setPreferredWidth(50);
                }
            }


            //Table table = new Table(); NOW!
            //add the table to the frame
            //add(new JScrollPane(table));
            c.fill = GridBagConstraints.HORIZONTAL;
            //c.ipady = 0;       //reset to default
            c.weighty = 0.0;   //request any extra vertical space
            //c.anchor = GridBagConstraints.PAGE_END; //bottom of space
            c.insets = new Insets(0, 0, 0, 0);  //top padding
            c.gridx = 0;//aligned with button 2
            //c.gridheight = 1;
            c.gridwidth = 3;   //2.5 columns wide
            c.gridy = 3;       //third row
            pane.add(scrollTable, c);

            listModel = new DefaultListModel();
            listModel.addElement("Academic Excellence");
            listModel.addElement("Freedom Fighter");
            listModel.addElement("Aboriginal/Minority Group");
            listModel.addElement("Best for NSU");
            listModel.addElement("Best for Student");

            //Create the list and put it in a scroll pane.
            list = new JList(listModel);
            list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            list.setLayoutOrientation(JList.VERTICAL_WRAP);
            list.setSelectedIndices(new int[] {}); // MIGHT BE WRONG -_-
            //list.addListSelectionListener(this);
            //list.addListSelectionListener((ListSelectionListener) this);
            list.setVisibleRowCount(5);
            JScrollPane listScrollPane = new JScrollPane(list);
            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridwidth = 1;
            c.gridx = 0;
            c.gridy = 4;
            pane.add(listScrollPane, c);

            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridx = 0;
            c.gridy = 5;
            pane.add(CalDiscountB, c);

            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridx = 1;
            c.gridy = 4;
            pane.add(Total, c);

            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridx = 1;
            c.gridy = 5;
            pane.add(Tax, c);

            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridx = 1;
            c.gridy = 6;
            pane.add(Discount, c);

            c.fill = GridBagConstraints.HORIZONTAL; //
            c.weightx = 0.0;
            c.insets = new Insets(10, 10, 0, 10);
            c.gridx = 1;
            c.gridy = 7;
            pane.add(GrandTotal, c);


        }
    }

    /*public class Table extends JPanel {
        public Table() {
            // COMMENTS ARE USED FOR YOUR CONVENIENT
            //headers for the table
            String[] columns = new String[]{
                    "SL#", "Course Title", "Credit", "Tuition/Credit", "Sub Total"
            };

            Registration registrationDb = rcc.getRegistration();
            int numOfRows = registrationDb.numOfCourse();

            Object[][] data = new Object[0][5];

            Course temp;
            int subtotal = 0;
            /*add previously added data into table
            for (int i=0;i<numOfRows;i++) {
                temp = registrationDb.getNextCourse();
                subtotal += temp.getSubTotal();

                data[i] = new Object[]{i+1,temp.getTitle(),temp.getCredit(),temp.getTuitionPerCredit(), temp.getSubTotal()};
            }*/

            //create table with data
            //tableModel = new DefaultTableModel(data, columns);
            //table = new JTable(tableModel);
            //Table table = new Table();

            //add the table to the frame
            //add(new JScrollPane(table));
            //table.setPreferredSize(new Dimension(700,600));
            //TableHorizontal tabhori = new TableHorizontal();
            //tabhori.TableHorizontal();
            //setSize(800,700);
            //table.setPreferredScrollableViewportSize(table.getPreferredSize());
            //table.setFillsViewportHeight(true);
            //table.setAutoResizeMode(5);
            //tableModel = (DefaultTableModel)table.getModel();

            //setTitle("Table Example");
            //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //
            //pack();
            //setVisible(true);

            //Changes a certain column size
            /*TableColumn column = null;
            for (int i = 0; i < 5; i++) {
                column = table.getColumnModel().getColumn(i);
                if (i == 1) {
                    column.setPreferredWidth(175); //third column is bigger
                } else {
                    column.setPreferredWidth(50);
                }
            }
        }
    }*/

    /*public static class TableHorizontal extends JFrame
    {
        public TableHorizontal()
        {
            final JTable table = new JTable(10, 5)
            {
                public boolean getScrollableTracksViewportWidth()
                {
                    return getPreferredSize().width < getParent().getWidth();
                }
            };
            table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF );
            final JScrollPane scrollPane = new JScrollPane( table );
            getContentPane().add( scrollPane );
        }

    }*/


    private class RegistrationButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            rcc.makeNewRegistration();
            tableModel.setRowCount(0);
            Total.setText("Total = 0");
            Tax.setText("Development Fee / BD Tax = 0");
            GrandTotal.setText("Grand Total = 0");
            Discount.setText("Discount = 0");
        }
    }
    private class NewCourseButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SecondFrame sec_frame = null;
            if(sec_frame == null) {
                sec_frame = new SecondFrame();
            }
            sec_frame.setVisible(true);
        }
    }

    private class CourseDesButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            IProgram res = rcc.getProgram(InputcourseTF.getText());
            Course crs = rcc.getCourse(InputcourseTF.getText());

            String output = "ID: " + crs.getId() + "\nTitle: " + crs.getTitle() + "\nCredit: " + crs.getCredit()
                    + "\nTuition: " + crs.getTuitionPerCredit() + "\n\nProgram name: " + res.getName()
                    + "\nDepartment: " + res.getDepartment();


            JFrame frame_2 = new JFrame();
            //if(frame_2 == null) {
                JOptionPane.showMessageDialog(frame_2,
                        output, //ami bujhchi nah eta kaj korbe nah. tomake experiment kore amake janate hobe. :(
                        "Course Details",            //.gettext() der ki append kora jabe nah string er bhitor? ektu bole janaba? eta
                        JOptionPane.INFORMATION_MESSAGE); //graphical dik theke shobcheye guchano tai notun frame baad diye jdialog korlam.
                                                            //JANAAAAAOOOOOOOOO! :(

            }
    //        frame_2.setVisible(true);
        }




    @Override
    public void onPropertyEvent(String name, int value) { //PUB-SUB
        if (name.equals("discount")) {
            grandTotal = value;
            GrandTotal.setText("Grand Total = " + grandTotal);
        } else if (name.equals("FreedomFighter")) {
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "You can't take more than five courses",
                    "Error!",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private class CalDiscountListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            rcc.getRegistration().setSelections(list.getSelectedIndices());

            Beeper beeper = new Beeper(rcc.getRegistration()); //PUB-SUB beeper listener

            rcc.getRegistration().getTotalWithDiscount(); //PUB-SUB event initialize

            rcc.getRegistration().checkFiveCourse();

            int discount = rcc.getRegistration().getGrandTotal()-grandTotal;
            Discount.setText("Discount = " +discount);

            //FREEDOM FIGHTER QUOTA CONDITIONS!
            // if more than 5 courses are chosen
            /*JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
                    "Now shake your tushy. :( ",
                    "I Love You <3",
                    JOptionPane.ERROR_MESSAGE);*/

            //System.exit(0); //WILL BE REMOVED
            //YOU CAN USE listModel.method AND list.method
            //You can work with getSelectedIndices() method
            //public int[] getSelectedIndices() etc etc

        }
    }

    public class AddCourseButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String courseId = InputcourseTF.getText();
            rcc.addCourse(courseId); //add into database

            Registration tempReg = rcc.getRegistration();
            Course temp = tempReg.getLastAdded();
            int serial = tempReg.getSerial();
            //add into table
            if(rcc.getCourse(courseId)!= null) {
                tableModel.addRow(new Object[]{serial, temp.getTitle(), temp.getCredit(), temp.getTuitionPerCredit(), temp.getSubTotal()});
            }

            String total = Integer.toString(tempReg.getTotal());
            Total.setText("Total = " + total);

            String tax = Integer.toString(tempReg.getExtraFeeAmount());
            Tax.setText("Development Fee / BD Tax = " +tax);

            String grandT = Integer.toString(tempReg.getGrandTotal());
            GrandTotal.setText("Grand Total = " + grandT);

           // System.exit(0);
        }
    }

    public class SecondFrame extends JFrame {
        public SecondFrame() {

            IdL = new JLabel("Course ID");
            TitleL = new JLabel("Course Title");
            CreditL = new JLabel("Course Credit");
            TuitionPerCreditL = new JLabel("Tuition/Credit");

            IdTF = new JTextField(10);
            TitleTF = new JTextField(10);
            CreditTF = new JTextField(10);
            TuitionperCreTF = new JTextField(10);

            ReturnToRegHandler rtrbHandler;
            ReturnToRegB = new JButton("Save & Return to Course Registration");
            rtrbHandler = new ReturnToRegHandler();
            ReturnToRegB.addActionListener(rtrbHandler);

            setTitle("New Course");

            GridBagLayoutForSF GBLFSF = new GridBagLayoutForSF();
            GBLFSF.addComponentsToPane(getContentPane());

            //tableModel = GBLD.getTableModel();

            setSize(700, 300);
            setVisible(true);
            setDefaultCloseOperation(EXIT_ON_CLOSE);


        }

        public class GridBagLayoutForSF {
            final static boolean shouldFill = true;
            final static boolean shouldWeightX = true;
            final static boolean RIGHT_TO_LEFT = false;
            //private DefaultTableModel tableModel; //

            public void addComponentsToPane(Container pane) {
                if (RIGHT_TO_LEFT) {
                    pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                }

                //JButton button;
                pane.setLayout(new GridBagLayout());
                GridBagConstraints c = new GridBagConstraints();
                if (shouldFill) {
                    //natural height, maximum width
                    c.fill = GridBagConstraints.HORIZONTAL;
                }

                //button = new JButton("Button 1");
                if (shouldWeightX) {
                    c.weightx = 0.0;
                }
                c.fill = GridBagConstraints.HORIZONTAL;
                c.insets = new Insets(10, 10, 10, 10);
                c.weightx = 0.0;
                c.gridx = 0;
                c.gridy = 0;
                pane.add(IdL, c);

                //button = new JButton("Button 2");
                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 0);
                c.gridx = 0;
                c.gridy = 1;
                pane.add(IdTF, c);

                //button = new JButton("Button 3");
                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;  // 0.0??
                //c.gridwidth = 2;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 1;
                c.gridy = 0;
                pane.add(TitleL, c);

                //button = new JButton("Long-Named Button 4");
                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 1;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 1;
                pane.add(TitleTF, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 2;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 0;
                pane.add(CreditL, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 2;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 1;
                pane.add(CreditTF, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 3;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 0;
                pane.add(TuitionPerCreditL, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 3;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 1;
                pane.add(TuitionperCreTF, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                //c.ipady = 40;      //make this component tall
                c.weightx = 0.0;
                c.insets = new Insets(10, 10, 10, 10);
                c.gridx = 4;
                //c.gridwidth = 2;   //2.5 columns wide
                c.gridy = 1;
                pane.add(ReturnToRegB, c);

            }
        }

        /*private class Return implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                rcc.makeNewRegistration();
                tableModel.setRowCount(0);
                Total.setText("Total = 0");
                Tax.setText("Development Fee / BD Tax = 0");
                GrandTotal.setText("Grand Total = 0");
                Discount.setText("Discount = 0");
            }
        }*/
        public class ReturnToRegHandler implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                Course c = new Course();

                //REPLACE THE HARCODED VALUES WITH THE REAL ONE
                c.setId(IdTF.getText());
                c.setTitle(TitleTF.getText());
                c.setCredit(Integer.parseInt(CreditTF.getText()));
                c.setTuitionPerCredit(Integer.parseInt(TuitionperCreTF.getText()));

                rcc.newCourse(c);

                dispose();
            }
        }
    }


        public static void main(String[] args) {
            GUI test = new GUI();
        }

}


