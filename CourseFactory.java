package AdvisingSystem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class CourseFactory {
    private LinkedList<Course> cList;
    private static CourseFactory instance; //
    private IExtraFeeCalculator efCalculator; //
    private String taxType;
    IMapper mapper = new CourseRDBMapper("course"); //TABLE NAME HERE
    private  ProgramProxy proxy; //PROXY

    CourseFactory() {
        cList = new LinkedList<>();
        Course course;

        //reading tax type from file
        String fileName = "taxmode.txt";
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fileReader);
            String line = br.readLine();
            br.close();

            if(line.equals("BDTax"))
                taxType = "AdvisingSystem.BDTaxAdapter";
            else if (line.equals("Development"))
                taxType = "AdvisingSystem.DevelopmentFeeCalculator";
            else System.out.println("Error reading file");
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
        }



        //hardcoded course entry in the database
        /*course = new Course();
        course.setId("CSE115");
        course.setTitle("Computing Concepts");
        course.setCredit(3);
        course.setTuitionPerCredit(4500);
        cList.add(course);

        course = new Course();
        course.setId("CSE115L");
        course.setTitle("Computing Concepts Lab");
        course.setCredit(1);
        course.setTuitionPerCredit(4500);
        cList.add(course);

        course = new Course();
        course.setId("CSE225");
        course.setTitle("Data Structure and Algorithm");
        course.setCredit(3);
        course.setTuitionPerCredit(4500);
        cList.add(course);

        course = new Course();
        course.setId("CSE225L");
        course.setTitle("Data Structure and Algorithm Lab");
        course.setCredit(1);
        course.setTuitionPerCredit(4500);
        cList.add(course);

        course = new Course();
        course.setId("CSE327");
        course.setTitle("Software Engineering");
        course.setCredit(3);
        course.setTuitionPerCredit(5500);
        cList.add(course);

        course = new Course();
        course.setId("PHY107");
        course.setTitle("Physics I");
        course.setCredit(3);
        course.setTuitionPerCredit(4500);
        cList.add(course);*/
    }

    public Course getCourse(String id) {
        Course search;
        int size = cList.size();
        boolean found = false;

        /*for (int i=0;i<size;i++) {
            search = cList.get(i);
            if(id.equals(search.getId())) {
                found = true;
                break;
            }
        }*/

        int oid = GenerateId(id); //generate hash code for OID

        search = (Course)mapper.get(oid); //MAY NOT WORK
        /*if (found) return search;
        else return null;*/
        return search;
    }

    //NEW DATA
    public void newCourse(Course course) {
        int oid = GenerateId(course.getId());

        mapper.put(oid,course);
    }
    private int GenerateId (String courseId) {
        int id = courseId.hashCode();

        return id;
    }

    //PROXY
    public IProgram getProgram(String courseId) {
        proxy = new ProgramProxy();
        int oid = GenerateId(courseId);
        Program result = new Program();

        proxy.searchOid(oid);

        result.setName(proxy.getName());
        result.setDepartment(proxy.getDepartment());

        return result;
    }

    public static synchronized CourseFactory getInstance() {
        if(instance == null)
            instance = new CourseFactory();

        return instance;
    }

    public IExtraFeeCalculator getExtraFeeCalculator() {
        if(efCalculator == null) {
            //String className = System.getProperty("java.util.LinkedList");
            //String className = "AdvisingSystem.BDTaxAdapter";
            try {
                efCalculator = (IExtraFeeCalculator) Class.forName(taxType).newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return efCalculator;
    }
}
